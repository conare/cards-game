package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type deck []string

func newDec() deck {
	cards := deck{}
	cardSuites := []string{"Spades", "Diamonds", "Hearts", "Club"}
	cardValues := []string{"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "K", "Q"}

	for _, suit := range cardSuites { //use underscore for variables that we do not use or use
		for _, value := range cardValues {
			cards = append(cards, suit+" of "+value)
		}
	}
	return cards
}

func (d deck) print() {
	for index, card := range d {
		fmt.Println(index, card)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

func readFromFile(filename string) deck {
	d, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("error is: ", err)
		os.Exit(1)
	}
	s := strings.Split(string(d), ",") // [] string
	return deck(s)
}
